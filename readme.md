## Synopsis

A simple Tic-Tac-Toe game for demonstration purposes

## Requirements
 Maven 3 and Java 8 is required
 
## Libraries
Project Lombok, Jackson, JUnit, Mockito

## Installation
1- Using the Command Line or Terminal, navigate to project root.  
2- Run these commands:  
```bash
mvn clean package
cd target
java -jar tictactoe-1.0-jar-with-dependencies.jar

```
##GamePlay
Game is played over the console. Every user turn, the current board status is printed
and if its a human players turn game will notify player with a message and wait for user console input.  

A coordinate input example: 1,2  

To exit the game, write **exit** through the prompt.
##Game Structure
####Configuration
* Game core configuration resides at ``` resources/game-config.xml``` 
```xml
<GameConfig>
    <humanPlayerCount>2</humanPlayerCount>
    <aiPlayerCount>1</aiPlayerCount>
    <possibleBoardSizes>3,4,5,6,7,8,9,10</possibleBoardSizes>
</GameConfig>

```
####Input
* Game input data resides at ``` resources/game-data.xml``` 
```xml
<GameData>
    <playerList>
        <PlayerData>
            <type>player</type>
            <name>Dinc</name>
            <cellValue>X</cellValue>
        </PlayerData>
        <PlayerData>
            <type>player</type>
            <name>Thomas</name>
            <cellValue>O</cellValue>
        </PlayerData>
        <PlayerData>
            <type>computer</type>
            <name>c3pO</name>
            <cellValue>A</cellValue>
        </PlayerData>
    </playerList>
    <boardSize>7</boardSize>
</GameData>

```
####Structure
* Game main class is ``` java/com/metroag/tictactoe/TicTacToeConsoleGame```
* Game is initialized as below. ```LoadManager```, ```InputManager``` and ```OutputManager``` were structured so that game can be loaded or displayed on other implemented environments as well.
```
TicTacToeGame.newGame(
    new XmlGameLoader(),
    new ConsoleInputManager(),
    new ConsoleOutputManager()
);
```
