package com.metroag.tictactoe.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This is created by Dinco-WORK on 11.12.2017.
 */
public class PointTest {

    @Test
    public void isEqualXYPointsIsEqualsReturnTrue(){
        Point p1 = new Point(1,2);
        Point p2 = new Point(1,2);
        assertTrue(p1.equals(p2));
    }

    @Test
    public void areEqualXYPointsHashCodesEqual(){
        Point p1 = new Point(1,2);
        Point p2 = new Point(1,2);
        assertEquals(p1.hashCode(), p2.hashCode());
    }

    @Test
    public void doesClonedObjectEqualsReturnFalse(){

    }
}