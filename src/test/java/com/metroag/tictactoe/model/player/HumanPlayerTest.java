package com.metroag.tictactoe.model.player;

import com.metroag.tictactoe.exception.InvalidOperationException;
import com.metroag.tictactoe.manager.input.ConsoleInputManager;
import com.metroag.tictactoe.manager.input.InputManager;
import com.metroag.tictactoe.manager.output.ConsoleOutputManager;
import com.metroag.tictactoe.manager.output.OutputManager;
import com.metroag.tictactoe.model.Point;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

/**
 * This is created by Dinco-WORK on 11.12.2017.
 */
public class HumanPlayerTest {

    InputManager inputManager = Mockito.mock(ConsoleInputManager.class);
    OutputManager outputManager = Mockito.mock(ConsoleOutputManager.class);

    @Test
    public void doesPlayerXYPointInputReturnXYPointResponseAction(){
        Mockito.when(inputManager.getUserInput()).thenReturn("0,1");
        Player player = new HumanPlayer(inputManager, outputManager, "Jack", "X");
        ActionResponse actionResponse = player.getPlayerAction();
        assertEquals(actionResponse.getSelectedPoint(), new Point(0,1));
    }

    @Test
    public void doesPlayerExitInputReturnExitResponseAction(){
        Mockito.when(inputManager.getUserInput()).thenReturn("exit");
        Player player = new HumanPlayer(inputManager, outputManager, "Jack", "X");
        ActionResponse actionResponse = player.getPlayerAction();
        assertEquals(actionResponse, ActionResponse.EXITED);
    }

    @Test(expected = InvalidOperationException.class)
    public void doesPlayerInvalidInputReturnException(){
        Mockito.when(inputManager.getUserInput()).thenReturn("1,a");
        Player player = new HumanPlayer(inputManager, outputManager, "Jack", "X");
        player.getPlayerAction();
    }
}