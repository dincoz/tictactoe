package com.metroag.tictactoe.model.cell;

import org.junit.Test;

import static org.junit.Assert.*;

public class CellValueTest {

    @Test(expected = RuntimeException.class)
    public void doesCellValuePreventNullInstantiation(){
        CellValue.newInstance(null);
    }

    @Test(expected = RuntimeException.class)
    public void doesCellValuePreventEmptyInstantiation(){
        CellValue.newInstance("");
    }

    @Test
    public void isEmptyConstantValueEmpty(){
        assertEquals(CellValue.Empty.getValue(), " ");
    }

    @Test
    public void isXInstanceValueEqualToX(){
        assertEquals(CellValue.newInstance("X").getValue(), "X");
    }

    @Test
    public void doesEqualValueCellComparisonReturnTrue(){
        CellValue cellValue1 = CellValue.newInstance("X");
        CellValue cellValue2 = CellValue.newInstance("X");
        assertTrue(cellValue1.equals(cellValue2));
    }

    @Test
    public void doesDifferentValueCellComparisonReturnFalse(){
        CellValue cellValue1 = CellValue.newInstance("X");
        CellValue cellValue2 = CellValue.newInstance("O");
        assertFalse(cellValue1.equals(cellValue2));
    }

    @Test
    public void areSameValueCellHashCodesEqual(){
        CellValue cellValue1 = CellValue.newInstance("X");
        CellValue cellValue2 = CellValue.newInstance("X");
        assertEquals(cellValue1.hashCode(), cellValue2.hashCode());
    }

    @Test
    public void areDifferentValueCellHashCodesNotEqual(){
        CellValue cellValue1 = CellValue.newInstance("X");
        CellValue cellValue2 = CellValue.newInstance("O");
        assertNotEquals(cellValue1.hashCode(), cellValue2.hashCode());
    }
}