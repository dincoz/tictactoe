package com.metroag.tictactoe.model.cell;

import org.junit.Test;

import static org.junit.Assert.*;

public class CellTest {

    @Test
    public void isEmptyInstanceStringValueEmpty(){
        Cell cell = Cell.emptyInstance();
        assertEquals(cell.getValue().toString(), " ");
    }

    @Test
    public void doesEmptyValueCellComparisonReturnTrue(){
        Cell cell1 = Cell.emptyInstance();
        Cell cell2 = Cell.emptyInstance();
        assertTrue(cell1.equals(cell2));
    }

    @Test
    public void doesSameValueCellComparisonReturnTrue(){
        Cell cell1 = Cell.emptyInstance();
        cell1.setValue(CellValue.newInstance("X"));
        Cell cell2 = Cell.emptyInstance();
        cell2.setValue(CellValue.newInstance("X"));
        assertTrue(cell1.equals(cell2));
    }

    @Test
    public void doesDifferentValueCellComparisonReturnFalse(){
        Cell cell1 = Cell.emptyInstance();
        cell1.setValue(CellValue.newInstance("X"));
        Cell cell2 = Cell.emptyInstance();
        cell2.setValue(CellValue.newInstance("O"));
        assertFalse(cell1.equals(cell2));
    }

    @Test
    public void areEmptyValueCellHashCodesEqual(){
        Cell cell1 = Cell.emptyInstance();
        Cell cell2 = Cell.emptyInstance();
        assertEquals(cell1.hashCode(), cell2.hashCode());
    }

    @Test
    public void areDifferentValueCellHashCodesDifferent(){
        Cell cell1 = Cell.emptyInstance();
        cell1.setValue(CellValue.newInstance("X"));
        Cell cell2 = Cell.emptyInstance();
        cell2.setValue(CellValue.newInstance("O"));
        assertNotEquals(cell1.hashCode(), cell2.hashCode());
    }

    @Test
    public void areSameValueCellHashCodesSame(){
        Cell cell1 = Cell.emptyInstance();
        cell1.setValue(CellValue.newInstance("X"));
        Cell cell2 = Cell.emptyInstance();
        cell2.setValue(CellValue.newInstance("X"));
        assertEquals(cell1.hashCode(), cell2.hashCode());
    }
}