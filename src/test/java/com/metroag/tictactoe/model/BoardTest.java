package com.metroag.tictactoe.model;

import com.metroag.tictactoe.exception.InvalidOperationException;
import com.metroag.tictactoe.model.cell.Cell;
import com.metroag.tictactoe.model.cell.CellValue;
import lombok.ToString;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This is created by Dinco-WORK on 11.12.2017.
 */
public class BoardTest {

    @Test
    public void areBoardCellsInitializedOnConstruction(){
        int boardSize = 3;
        Board board = new Board(boardSize);
        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {
                assertNotNull(board.getCellValue(x, y));
            }
        }
    }

    @Test
    public void doesMarkPointSetPoint(){
        int boardSize = 3;
        Point oneOne = new Point( 1, 1);
        CellValue xValue = CellValue.newInstance("X");
        Board board = new Board(boardSize);
        board.markPoint(oneOne, xValue);
        assertEquals(board.getCellValue(oneOne), xValue);
    }

    @Test
    public void doesResetClearPoints(){
        int boardSize = 3;
        Point oneOne = new Point( 1, 1);
        CellValue xValue = CellValue.newInstance("X");
        Board board = new Board(boardSize);
        board.markPoint(oneOne, xValue);
        board.reset();
        assertEquals(board.getCellValue(oneOne), CellValue.Empty);
    }

    @Test
    public void doesMinusMinusPointValidationReturnFalse(){
        int boardSize = 3;
        Point minusMinus = new Point( -1, -1);
        Board board = new Board(boardSize);
        assertFalse(board.isValidPoint(minusMinus));
    }

    @Test
    public void doesBoardSizeExceedPointValidationReturnFalse(){
        int boardSize = 3;
        Point minusMinus = new Point( 4, 4);
        Board board = new Board(boardSize);
        assertFalse(board.isValidPoint(minusMinus));
    }

    @Test
    public void doesIsEmptyPointCheckForEmptyPointReturnTrue(){
        int boardSize = 3;
        Point point = new Point( 4, 4);
        Board board = new Board(boardSize);
        assertFalse(board.isEmptyPoint(point));
    }

    @Test(expected = InvalidOperationException.class)
    public void doesIsEmptyPointCheckForFullPointReturnFalse(){
        int boardSize = 3;
        Point point = new Point( 4, 4);
        Board board = new Board(boardSize);
        board.markPoint(point, CellValue.newInstance("X"));
    }
}