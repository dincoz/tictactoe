package com.metroag.tictactoe.action;

/**
 * This is created by Dinco-WORK on 11.12.2017.
 */
public interface Action {
    void run();
}
