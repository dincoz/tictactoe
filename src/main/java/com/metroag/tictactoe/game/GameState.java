package com.metroag.tictactoe.game;

import lombok.Data;

/**
 * This is created by Dinco-WORK on 11.12.2017.
 */
@Data
public class GameState {
    private boolean userWon;
    private boolean userExited;
    private boolean draw;

    public boolean continues(){
        return !userWon && !draw && !userExited;
    }
}
