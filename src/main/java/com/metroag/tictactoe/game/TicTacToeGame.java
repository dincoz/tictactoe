package com.metroag.tictactoe.game;

import com.metroag.tictactoe.configuration.GameConfiguration;
import com.metroag.tictactoe.configuration.GameData;
import com.metroag.tictactoe.exception.InvalidOperationException;
import com.metroag.tictactoe.manager.AIManager;
import com.metroag.tictactoe.manager.CalculationManager;
import com.metroag.tictactoe.manager.input.InputManager;
import com.metroag.tictactoe.manager.loader.ConfigurationLoader;
import com.metroag.tictactoe.manager.output.OutputManager;
import com.metroag.tictactoe.manager.loader.game.GameLoader;
import com.metroag.tictactoe.model.player.ActionResponse;
import com.metroag.tictactoe.model.Board;
import com.metroag.tictactoe.model.Point;
import com.metroag.tictactoe.model.player.Player;
import com.metroag.tictactoe.model.player.factory.PlayerFactoryManager;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
@Getter
public class TicTacToeGame {

    private List<Player> players;
    private Board board;
    private GameState gameState;

    private GameLoader gameLoader;
    private InputManager inputManager;
    private OutputManager outputManager;
    private CalculationManager calculationManager;
    private AIManager aiManager;

    private PlayerFactoryManager playerFactoryManager;
    private Integer currentPlayerIndex;

    public static void newGame(
            GameLoader gameLoader,
            InputManager inputManager,
            OutputManager outputManager
    ){
        TicTacToeGame game = new TicTacToeGame(gameLoader, inputManager, outputManager);
        game.load();
        game.start();
    }

    private TicTacToeGame(
            GameLoader gameLoader,
            InputManager inputManager,
            OutputManager outputManager) {
        this.gameLoader = gameLoader;
        this.inputManager = inputManager;
        this.outputManager = outputManager;
    }

    private void load(){
        this.gameState = new GameState();
        this.currentPlayerIndex = 0;
        GameConfiguration gameConfiguration = ConfigurationLoader.getInstance().loadConfiguration();
        GameData gameData = gameLoader.loadGame(gameConfiguration);
        this.board = new Board(gameData.getBoardSize());

        this.outputManager.setBoard(board);
        this.calculationManager = new CalculationManager(board);
        this.aiManager = new AIManager(board);
        this.playerFactoryManager = PlayerFactoryManager.getInstance(inputManager, outputManager, aiManager);
        this.players = gameData.getPlayerList()
                .stream()
                .map(playerData ->
                        playerFactoryManager
                            .getFactoryByType(playerData.getType())
                            .newPlayerFromData(playerData))
                .collect(Collectors.toList());
    }

    private void start(){
        outputManager.printMessage("Game Started...");
        outputManager.printBoard();

        while(gameState.continues()){
            try{
                ActionResponse actionResponse = getCurrentPlayer().getPlayerAction();
                if(actionResponse == ActionResponse.EXITED){
                    gameState.setUserExited(true);
                    outputManager.printMessage("Game exited...");
                }
                else {
                    completePlayerTurnAction(actionResponse.getSelectedPoint());
                }
            }
            catch(InvalidOperationException e){
                outputManager.printMessage(e.getMessage());
            }

        }
    }

    private void completePlayerTurnAction(Point selectedPoint){
        board.markPoint(
                selectedPoint,
                getCurrentPlayer().getCellValue()
        );
        gameState.setUserWon(calculationManager.userHasWon(
                selectedPoint,
                getCurrentPlayer().getCellValue()
        ));

        if(!gameState.isUserWon()){
            gameState.setDraw(board.isBoardCellsFilled());
        }

        outputManager.printBoard();

        if(gameState.isUserWon()) {
            outputManager.printMessage(getCurrentPlayer().getName() + " has won!");
        }
        else if(gameState.isDraw())  {
            outputManager.printMessage("Game is draw!");
        }
        else {
            setNextPlayer();
        }
    }

    private void setNextPlayer(){
        currentPlayerIndex++;
        if(currentPlayerIndex >= players.size()){
            currentPlayerIndex = 0;
        }
    }

    private Player getCurrentPlayer(){
        return players.get(currentPlayerIndex);
    }
}
