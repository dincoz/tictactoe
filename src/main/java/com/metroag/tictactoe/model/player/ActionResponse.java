package com.metroag.tictactoe.model.player;

import com.metroag.tictactoe.model.Point;
import lombok.Data;

/**
 * This is created by Dinco-WORK on 11.12.2017.
 */
@Data
public class ActionResponse {
    public static final String EXIT = "exit";
    public static final ActionResponse EXITED = new ActionResponse();

    private Point selectedPoint;
    private boolean userExited;

    private ActionResponse() {
        this.userExited = true;
    }

    public ActionResponse(Point selectedPoint) {
        this.selectedPoint = selectedPoint;
    }
}
