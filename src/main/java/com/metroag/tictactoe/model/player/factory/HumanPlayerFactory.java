package com.metroag.tictactoe.model.player.factory;

import com.metroag.tictactoe.configuration.PlayerData;
import com.metroag.tictactoe.manager.input.InputManager;
import com.metroag.tictactoe.manager.output.OutputManager;
import com.metroag.tictactoe.model.player.HumanPlayer;
import com.metroag.tictactoe.model.player.Player;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class HumanPlayerFactory implements PlayerFactory {
    private InputManager inputManager;
    private OutputManager outputManager;

    public HumanPlayerFactory(InputManager inputManager, OutputManager outputManager) {
        this.inputManager = inputManager;
        this.outputManager = outputManager;
    }

    public Player newPlayerFromData(PlayerData playerData) {
        return new HumanPlayer(inputManager, outputManager, playerData.getName(), playerData.getCellValue());
    }
}
