package com.metroag.tictactoe.model.player;

import com.metroag.tictactoe.exception.InvalidOperationException;
import com.metroag.tictactoe.manager.input.InputManager;
import com.metroag.tictactoe.manager.output.OutputManager;
import com.metroag.tictactoe.model.Point;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */

public class HumanPlayer extends Player{
    private InputManager inputManager;
    private OutputManager outputManager;

    public HumanPlayer(InputManager inputManager, OutputManager outputManager, String name, String cellValue) {
        super(name, cellValue);
        this.inputManager = inputManager;
        this.outputManager = outputManager;
    }

    @Override
    public ActionResponse getPlayerAction() {
        outputManager.printMessage("Player " + this.getName() + ", enter coordinate (x,y):\n");
        String input = inputManager.getUserInput();
        if(input.equalsIgnoreCase(ActionResponse.EXIT)){
            return ActionResponse.EXITED;
        }
        try{
            List<Integer> point = Arrays.stream(input.split(","))
                    .map(String::trim)
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            if(point.size() < 2)
                throw new InvalidOperationException("Wrong coordinate input format");
            return new ActionResponse(new Point(point.get(0), point.get(1)));
        }
        catch (NumberFormatException e){
            throw new InvalidOperationException("Wrong coordinate input format");
        }
    }
}
