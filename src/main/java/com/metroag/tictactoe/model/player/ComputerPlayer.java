package com.metroag.tictactoe.model.player;

import com.metroag.tictactoe.manager.AIManager;
import com.metroag.tictactoe.manager.output.OutputManager;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class ComputerPlayer extends Player{
    private AIManager aiManager;
    private OutputManager outputManager;

    public ComputerPlayer(AIManager aiManager, OutputManager outputManager, String name, String cellValue) {
        super(name, cellValue);
        this.aiManager = aiManager;
        this.outputManager = outputManager;
    }

    @Override
    public ActionResponse getPlayerAction() {
        outputManager.printMessage("Computer " + this.getName() + " (" + getCellValue() + ") is playing");
        return new ActionResponse(aiManager.getNextCoordinates());
    }
}
