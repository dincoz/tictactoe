package com.metroag.tictactoe.model.player;

import com.metroag.tictactoe.model.cell.CellValue;
import lombok.Data;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
@Data
public abstract class Player {
    private String name;
    private CellValue cellValue;

    public Player(String name, String cellValue) {
        this.name = name;
        this.cellValue = CellValue.newInstance(cellValue);
    }

    public abstract ActionResponse getPlayerAction();
}
