package com.metroag.tictactoe.model.player.factory;

import com.metroag.tictactoe.configuration.PlayerData;
import com.metroag.tictactoe.manager.AIManager;
import com.metroag.tictactoe.manager.output.OutputManager;
import com.metroag.tictactoe.model.player.ComputerPlayer;
import com.metroag.tictactoe.model.player.Player;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class AIPlayerFactory implements PlayerFactory {
    private AIManager aiManager;
    private OutputManager outputManager;

    public AIPlayerFactory(AIManager aiManager, OutputManager outputManager) {
        this.aiManager = aiManager;
        this.outputManager = outputManager;
    }

    @Override
    public Player newPlayerFromData(PlayerData playerData) {
        return new ComputerPlayer(aiManager, outputManager, playerData.getName(), playerData.getCellValue());
    }
}
