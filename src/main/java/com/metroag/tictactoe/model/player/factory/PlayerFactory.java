package com.metroag.tictactoe.model.player.factory;

import com.metroag.tictactoe.configuration.PlayerData;
import com.metroag.tictactoe.model.player.Player;

/**
 * This is created by Dinco-WORK on 10.12.2017.
 */
public interface PlayerFactory {
    Player newPlayerFromData(PlayerData playerData);
}
