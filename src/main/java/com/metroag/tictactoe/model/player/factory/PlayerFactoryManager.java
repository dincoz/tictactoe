package com.metroag.tictactoe.model.player.factory;

import com.metroag.tictactoe.constants.PlayerType;
import com.metroag.tictactoe.manager.AIManager;
import com.metroag.tictactoe.manager.input.InputManager;
import com.metroag.tictactoe.manager.output.OutputManager;

import java.util.HashMap;
import java.util.Map;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class PlayerFactoryManager {
    private static PlayerFactoryManager instance;
    public static PlayerFactoryManager getInstance(InputManager inputManager, OutputManager outputManager, AIManager aiManager) {
        if (instance == null)
            instance = new PlayerFactoryManager(inputManager, outputManager, aiManager);
        return instance;
    }

    private PlayerFactoryManager(InputManager inputManager, OutputManager outputManager, AIManager aiManager){
        factories = new HashMap<String, PlayerFactory>() {{
            put(PlayerType.PLAYER.getName(), new HumanPlayerFactory(inputManager, outputManager));
            put(PlayerType.COMPUTER.getName(), new ComputerPlayerFactory(aiManager, outputManager));
        }};
    }

    public Map<String, PlayerFactory> factories;

    public PlayerFactory getFactoryByType(String type){
        return factories.get(type);
    }

}
