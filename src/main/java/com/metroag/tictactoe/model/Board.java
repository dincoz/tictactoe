package com.metroag.tictactoe.model;

import com.metroag.tictactoe.exception.InvalidOperationException;
import com.metroag.tictactoe.model.cell.Cell;
import com.metroag.tictactoe.model.cell.CellValue;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class Board {
    private Integer boardSize;
    private Cell[][] cells;

    private Integer cellCount;
    private Integer fullCellCount;

    public Board(Integer boardSize) {
        initialize(boardSize);
    }

    private void initialize(Integer boardSize){
        this.boardSize = boardSize;
        this.cells = new Cell[boardSize][boardSize];
        this.cellCount = boardSize * boardSize;
        reset();
    }

    public void reset(){
        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {
                cells[x][y] = Cell.emptyInstance();
            }
        }
        fullCellCount = 0;
    }

    public void validateCoordinateToMark(Point point){
        if(!isEmptyPoint(point)){
            throw new InvalidOperationException("Please enter a valid coordinate");
        }
    }

    public boolean isEmptyPoint(Point point){
        return isValidPoint(point) && cells[point.getX()][point.getY()].getValue() == CellValue.Empty;
    }

    public boolean isValidPoint(Point point){
        return point.getX() >= 0 && point.getY() >= 0 &&
                point.getX() < boardSize && point.getY() < boardSize;
    }

    public void markPoint(Point point, CellValue value){
        validateCoordinateToMark(point);
        cells[point.getX()][point.getY()].setValue(value);
        fullCellCount++;
    }

    public Integer getBoardSize() {
        return boardSize;
    }

    public CellValue getCellValue(int x, int y) {
        return cells[x][y].getValue();
    }

    public CellValue getCellValue(Point point) {
        return cells[point.getX()][point.getY()].getValue();
    }

    public Boolean isBoardCellsFilled() {
        return fullCellCount >= cellCount;
    }
}
