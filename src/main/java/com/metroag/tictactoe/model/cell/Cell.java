package com.metroag.tictactoe.model.cell;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class Cell {
    public static final Cell Empty = emptyInstance();
    public static Cell emptyInstance() {
        return new Cell(CellValue.Empty);
    }

    @Getter
    @Setter
    private CellValue value;

    private Cell(CellValue value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return Objects.equals(value, cell.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value.getValue());
    }
}
