package com.metroag.tictactoe.model.cell;

import lombok.Data;

import java.util.Objects;
import java.util.Optional;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
@Data
public class CellValue {
    public static final CellValue Empty = emptyInstance();

    private String value;

    private CellValue() { }

    private static CellValue emptyInstance(){
        CellValue cellValue = new CellValue();
        cellValue.setValue(" ");
        return cellValue;
    }

    public static CellValue newInstance(String value){
        CellValue cellValue = new CellValue();
        cellValue.initialize(value);
        return cellValue;
    }

    private void initialize(String value){
        boolean valueIsValid = Optional.ofNullable(value)
                .map(String::trim)
                .map(string -> !value.isEmpty())
                .orElse(false);
        if(!valueIsValid){
            throw new RuntimeException("Cell value can't be null or empty");
        }
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CellValue cellValue = (CellValue) o;
        return Objects.equals(value, cellValue.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
