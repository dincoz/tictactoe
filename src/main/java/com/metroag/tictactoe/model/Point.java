package com.metroag.tictactoe.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * This is created by Dinco-WORK on 10.12.2017.
 */
@Getter
@Setter
public class Point {
    public static final Point ZERO_ZERO = new Point(0, 0);

    private Integer x;
    private Integer y;

    public Point(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Point add(Point delta){
        this.x += delta.x;
        this.y += delta.y;
        return this;
    }
    public Point clone(){
        return new Point(x, y);
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Objects.equals(x, point.x) &&
                Objects.equals(y, point.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
