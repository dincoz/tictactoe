package com.metroag.tictactoe.constants;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public enum PlayerType {
    PLAYER("player"), COMPUTER("computer");

    private String name;

    PlayerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
