package com.metroag.tictactoe.configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
@Getter
@Setter
public class GameConfiguration {
    private int humanPlayerCount;
    private int aiPlayerCount;
    private String possibleBoardSizes;
}
