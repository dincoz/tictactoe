package com.metroag.tictactoe.configuration;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
@Setter
@Getter
public class GameData {
    private List<PlayerData> playerList;
    private Integer boardSize;
}
