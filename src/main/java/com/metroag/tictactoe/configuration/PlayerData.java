package com.metroag.tictactoe.configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
@Getter
@Setter
public class PlayerData {
    private String type;
    private String name;
    private String cellValue;
}
