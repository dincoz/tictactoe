package com.metroag.tictactoe;

import com.metroag.tictactoe.game.TicTacToeGame;
import com.metroag.tictactoe.manager.input.ConsoleInputManager;
import com.metroag.tictactoe.manager.output.ConsoleOutputManager;
import com.metroag.tictactoe.manager.loader.game.XmlGameLoader;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class TicTacToeConsoleGame {
    public static void main(String[] args) {
        TicTacToeGame.newGame(
                new XmlGameLoader(),
                new ConsoleInputManager(),
                new ConsoleOutputManager()
        );
    }
}
