package com.metroag.tictactoe.exception;

/**
 * This is created by Dinco-WORK on 10.12.2017.
 */
public class InvalidOperationException extends RuntimeException {
    public InvalidOperationException(String message) {
        super(message);
    }
}
