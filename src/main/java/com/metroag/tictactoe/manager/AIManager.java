package com.metroag.tictactoe.manager;

import com.metroag.tictactoe.model.Board;
import com.metroag.tictactoe.model.Point;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * This is created by Dinco-WORK on 10.12.2017.
 */
public class AIManager {
    private Board board;
    private Random r = new Random();

    public AIManager(Board board) {
        this.board = board;
    }

    /***
     * Generates Point from an empty cell for demo purposes
     * @return
     */
    public Point getNextCoordinates(){
        Point point;
        do {
            point = new Point(r.nextInt(board.getBoardSize()), r.nextInt(board.getBoardSize()));
        }
        while(!board.isEmptyPoint(point));
        try {
            // To give a sense of real person thinking
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return point;
    }
}
