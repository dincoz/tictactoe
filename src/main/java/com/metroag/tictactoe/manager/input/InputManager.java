package com.metroag.tictactoe.manager.input;

import com.metroag.tictactoe.model.Point;
import com.metroag.tictactoe.model.player.ActionResponse;

/**
 * This is created by Dinco-WORK on 10.12.2017.
 */
public interface InputManager {
    String getUserInput();
}
