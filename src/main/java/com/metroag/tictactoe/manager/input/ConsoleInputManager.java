package com.metroag.tictactoe.manager.input;

import com.metroag.tictactoe.exception.InvalidOperationException;
import com.metroag.tictactoe.model.Point;
import com.metroag.tictactoe.model.player.ActionResponse;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * This is created by Dinco-WORK on 10.12.2017.
 */
public class ConsoleInputManager implements InputManager {
    private Scanner scanner= new Scanner(System.in);

    @Override
    public String getUserInput(){
        return scanner.next();
    }

}
