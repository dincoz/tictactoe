package com.metroag.tictactoe.manager.output;

import com.metroag.tictactoe.model.Board;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class ConsoleOutputManager implements OutputManager {
    private Board board;

    @Override
    public void printBoard() {
        System.out.print("\n   ");
        for (int x = 0; x < board.getBoardSize(); x++) {
            System.out.print(" " + x + " ");
        }
        for (int y = 0; y < board.getBoardSize(); y++) {
            System.out.println();
            System.out.print(" " + y + " ");
            for (int x = 0; x < board.getBoardSize(); x++) {
                printCell(board, x, y);
            }
        }
        System.out.println();
    }

    @Override
    public void printMessage(String message) {
        System.out.print("\n" + message + "\n");
    }

    private void printCell(Board board, int x, int y){
        System.out.print("[" + board.getCellValue(x, y) + "]");
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }
}
