package com.metroag.tictactoe.manager.output;

import com.metroag.tictactoe.model.Board;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public interface OutputManager {
    void printBoard();
    void setBoard(Board board);

    void printMessage(String message);
}
