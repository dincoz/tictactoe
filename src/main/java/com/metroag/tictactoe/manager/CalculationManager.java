package com.metroag.tictactoe.manager;

import com.metroag.tictactoe.model.Board;
import com.metroag.tictactoe.model.Point;
import com.metroag.tictactoe.model.cell.CellValue;

import java.util.Arrays;
import java.util.List;

/**
 * This is created by Dinco-WORK on 10.12.2017.
 */
public class CalculationManager {
    private Board board;
    private CellValue userCellValue;
    private boolean userHasWon;

    private List<Direction> directions;

    public CalculationManager(Board board) {
        this.board = board;
        initializeDirections();
    }

    private void initializeDirections(){
        Direction left = new Direction("left", -1, 0);
        Direction right = new Direction("right",1, 0);
        Direction top = new Direction("top",0, -1);
        Direction bottom = new Direction("bottom",0, 1);
        Direction topLeft = new Direction("topLeft",-1, -1);
        Direction topRight = new Direction("topRight",1, -1);
        Direction bottomLeft = new Direction("bottomLeft",-1, 1);
        Direction bottomRight = new Direction("bottomRight",1, 1);

        left.setOppositeDirection(right);
        top.setOppositeDirection(bottom);
        topLeft.setOppositeDirection(bottomRight);
        topRight.setOppositeDirection(bottomLeft);
        right.setOppositeDirection(left);
        bottom.setOppositeDirection(top);
        bottomRight.setOppositeDirection(topLeft);
        bottomLeft.setOppositeDirection(topRight);

        this.directions = Arrays.asList(left, top, topLeft, topRight, right, bottom, bottomRight, bottomLeft);
    }

    public boolean userHasWon(Point userCurrentPoint, CellValue userCellValue){
        this.userCellValue = userCellValue;
        calculate(userCurrentPoint, 0);
        return userHasWon;
    }

    public void calculate(Point currentPoint, int directionIndex){
        while(directionIndex < directions.size()){
            int directionLevel = 0;
            directionLevel = calcCurrentDirectionAdjLevel(currentPoint, directions.get(directionIndex), directionLevel);
            if(directionLevel == 1){
                directionLevel = calcCurrentDirectionAdjLevel(currentPoint, directions.get(directionIndex).oppositeDirection, directionLevel);
            }
            if(directionLevel == 2){
                userHasWon = true;
                return;
            }
            directionIndex++;
        }
    }

    private int calcCurrentDirectionAdjLevel(Point point, Direction direction, int directionLevel){
        Point tempPoint = point.clone().add(direction.delta);
        if(pointHasUserCellValue(tempPoint)){
            return directionLevel + getDirectionLevel(tempPoint, direction, 1);
        }
        return directionLevel;
    }

    private int getDirectionLevel(Point point, Direction direction, int level){
        Point tempPoint = point.clone().add(direction.delta);
        if(pointHasUserCellValue(tempPoint)){
            return getDirectionLevel(tempPoint, direction, ++level);
        }
        return level;
    }

    private boolean pointHasUserCellValue(Point point){
        return board.isValidPoint(point) &&
                board.getCellValue(point).equals(userCellValue);
    }

    private class Direction {
        private String name;
        private Point delta;
        private Direction oppositeDirection;

        public Direction(String name, Integer dX, Integer dY) {
            this.delta = new Point(dX, dY);
            this.name = name;
        }

        public void setOppositeDirection(Direction oppositeDirection) {
            this.oppositeDirection = oppositeDirection;
        }

        @Override
        public String toString() {
            return name;
        }
    }

}