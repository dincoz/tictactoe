package com.metroag.tictactoe.manager.loader.game;

import com.metroag.tictactoe.configuration.GameData;
import com.metroag.tictactoe.manager.loader.FileLoader;

import java.io.IOException;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class XmlGameLoader extends AbstractGameLoader {
    FileLoader<GameData> fileLoader;

    public XmlGameLoader(){
        this.fileLoader = new FileLoader<>();
    }

    public GameData loadGameData() {
        GameData gameData = null;
        try {
            gameData = fileLoader.loadFile("/game-data.xml", GameData.class);
        } catch (IOException e) {
            throw new RuntimeException("Game Data file cannot be loaded successfully");
        }
        return gameData;
    }
}
