package com.metroag.tictactoe.manager.loader.game;

import com.metroag.tictactoe.constants.PlayerType;
import com.metroag.tictactoe.configuration.GameConfiguration;
import com.metroag.tictactoe.configuration.GameData;
import com.metroag.tictactoe.configuration.PlayerData;
import com.metroag.tictactoe.manager.loader.game.GameLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is created by Dinco-WORK on 10.12.2017.
 */
public abstract class AbstractGameLoader implements GameLoader {

    protected abstract GameData loadGameData();

    @Override
    public GameData loadGame(GameConfiguration gameConfiguration) {
        GameData gameData = loadGameData();
        List<PlayerData> playerList = new ArrayList<>();
        int aiCount = 0, humanCount = 0;
        StringBuffer errorMessage = new StringBuffer();
        gameData.setPlayerList(gameData.getPlayerList()
                .stream()
                .filter(playerData ->
                        PlayerType.PLAYER.getName().equals(playerData.getType()) ||
                                PlayerType.COMPUTER.getName().equals(playerData.getType())
                )
                .collect(Collectors.toList())
        );
        for (PlayerData playerData : gameData.getPlayerList()) {
            if(PlayerType.PLAYER.getName().equals(playerData.getType())){
                humanCount++;
            } else if(PlayerType.COMPUTER.getName().equals(playerData.getType())){
                aiCount++;
            }
        }
        if(aiCount != gameConfiguration.getAiPlayerCount()){
            errorMessage.append("\nTotal Computer Player count must be " + gameConfiguration.getAiPlayerCount());
        }
        if(humanCount != gameConfiguration.getHumanPlayerCount()){
            errorMessage.append("\nTotal Human Player count must be " + gameConfiguration.getHumanPlayerCount());
        }
        long validBoardSizes =
                Arrays.stream(gameConfiguration.getPossibleBoardSizes().split(","))
                        .map(Integer::parseInt)
                        .filter(boardSize -> boardSize.equals(gameData.getBoardSize()))
                        .count();
        if(validBoardSizes < 1){
            errorMessage.append("\nPossible board sizes are " + gameConfiguration.getPossibleBoardSizes());
        }
        if(errorMessage.length() > 0) {
            throw new RuntimeException(errorMessage.toString());
        }
        return gameData;
    }
}
