package com.metroag.tictactoe.manager.loader;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.*;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class FileLoader<T> {

    public T loadFile(String path, Class<T> contentClass) throws IOException {
        InputStream inputStream = getClass().getResourceAsStream(path);
        XmlMapper xmlMapper = new XmlMapper();
        String xml = inputStreamToString(inputStream);
        T value = xmlMapper.readValue(xml, contentClass);
        return value;
    }

    public String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
