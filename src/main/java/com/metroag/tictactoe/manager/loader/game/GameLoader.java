package com.metroag.tictactoe.manager.loader.game;

import com.metroag.tictactoe.configuration.GameConfiguration;
import com.metroag.tictactoe.configuration.GameData;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public interface GameLoader {
    GameData loadGame(GameConfiguration gameConfiguration);
}
