package com.metroag.tictactoe.manager.loader;

import com.metroag.tictactoe.configuration.GameConfiguration;

import java.io.IOException;

/**
 * This is created by Dinco-WORK on 9.12.2017.
 */
public class ConfigurationLoader {
    FileLoader<GameConfiguration> fileLoader;

    private static ConfigurationLoader instance;
    public static ConfigurationLoader getInstance() {
        if (instance == null)
            instance = new ConfigurationLoader();
        return instance;
    }

    private ConfigurationLoader(){
        this.fileLoader = new FileLoader<>();
    }

    public GameConfiguration loadConfiguration() {
        GameConfiguration configuration = null;
        try {
            configuration = fileLoader.loadFile("/game-config.xml", GameConfiguration.class);
        } catch (IOException e) {
            throw new RuntimeException("Configuration file cannot be loaded successfully");
        }
        return configuration;
    }
}
